import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';



export default class about extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.title}>About me</Text>
                <Image
                    style={styles.logo}
                    source={require('./image/user.png')}
                />
                <Text style={styles.title}>Arna Muhammad Rasyid</Text>
                <Text style={styles.title}>React Native Developer</Text>
                <View style={styles.itemBox}>
                    <Text style={styles.title}>Portofolio</Text>
                    <Image
                        style={styles.logoItem}
                        source={require('./image/gitlab-icon-28.png')}
                    />
                    <Text style={styles.textItem}>ociddd79</Text>
                </View>
                <View style={styles.itemBox}>
                    <Text style={styles.title}>Social Media</Text>
                    <Image
                        style={styles.logoItem}
                        source={require('./image/instagram.png')}
                    />
                    <Text style={styles.textItem}>ociddd79</Text>
                    <Image
                        style={styles.logoItem}
                        source={require('./image/twitter.png')}
                    />
                    <Text style={styles.textItem}>ociddd79</Text>
                </View>
            </View>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#393E46",
        flex: 1
    },
    logo: {
        width: 150,
        height: 150
    },
    logoItem: {
        marginTop: 30,
        width: 50,
        height: 50
    },
    textItem: {
        marginTop: -45,
        marginLeft: 60,
        color: '#eeeeee',
        fontSize: 14,
        padding: 8
    },
    title: {
        color: '#eeeeee',
        fontSize: 20,
        padding: 10
    },
    text: {
        color: '#eeeeee',
        fontSize: 18,
        padding: 10
    },
    itemBox: {
        padding: 10

    }

})