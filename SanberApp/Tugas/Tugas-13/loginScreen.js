import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, Button } from 'react-native';

import About from './about';

export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.logo}
                    source={require("./image/logo.png")}
                />
                <Text style={styles.title}>Renown</Text>
                <Text style={styles.text}>Username</Text>
                <View style={styles.textBox}></View>
                <Text style={styles.text}>Password</Text>
                <View style={styles.textBox}></View>
                <View style={styles.button}>
                    <Text style={styles.buttonText}>Login</Text>
                </View>

            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#393E46",
        flex: 1
    },
    logo: {
        width: 150,
        height: 150
    },
    title: {
        color: '#eeeeee',
        fontSize: 20,
        padding: 10
    },
    text: {
        color: '#eeeeee',
        fontSize: 18,
        padding: 10
    },
    textBox: {
        width: 300,
        height: 48,
        backgroundColor: '#eeeeee',
        borderWidth: 2,
        borderColor: '#ffd369',
        borderRadius: 20

    },
    button: {
        width: 150,
        height: 48,
        backgroundColor: '#eeeeee',
        borderWidth: 2,
        borderColor: '#ffd369',
        borderRadius: 20,
        margin: 20
    },
    buttonText: {
        color: '#393E46',
        fontSize: 20,
        padding: 10,
        textAlign: 'center'
    }
})