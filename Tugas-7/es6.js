console.log("soal 1\n")

const golden = goldenFunction = () => {
		console.log("This is golden!")
	}

golden()

console.log("\nsoal 2\n")

const newFunction = literal = (firstName, lastName ) =>{
	return{
		firstName: firstName,
		lastName: lastName,
		fullName: function2 = () => {
			console.log(firstName+" "+lastName)
			return
		}
	}
}
newFunction("William","imoh").fullName()

console.log("\nsoal 3\n")

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName, destination, occupation} = newObject
console.log(firstName, lastName, destination, occupation)

console.log("\nsoal 4\n")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [west, east]
//Driver Code
console.log(combined)

console.log("\nsoal 5\n")

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view} dolor sit amet,   
    consectetur adipiscing elit, ${planet}do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`
 
// Driver Code
console.log(before) 