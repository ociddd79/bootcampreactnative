console.log("Soal 1")

var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr){
	
	if(arr.length<0){
		console.log("")
	}

	for(var i = 0; i<arr.length; i++){
		var obj ={}
		obj.firstName = arr[i][0]
		obj.lastName = arr[i][1]
		obj.gender = arr[i][2]
		obj.birthYear = arr[i][3]
		if(obj.birthYear >=0 && thisYear - obj.birthYear>= 0 ){
			obj.Age = thisYear - obj.birthYear
		}else(
			obj.Age = "Invalid"
		)
		var output = (i+1) + '. ' +obj.firstName+''+obj.lastName+" :{\nfirst name : "+obj.firstName+"\nlast name : "+obj.lastName+"\ngender : "+obj.gender+"\nAge : "+obj.Age+"\n}"
		console.log(output)
	}

	
	
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)


var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

console.log("\nSoal 2\n")

function shoppingTime(memberId, money){
	if(!memberId){
		return "Maaf toko khusus member saja"
	}else if(money < 50000){
		return "maaf uang tidak cukup"
	}else{
		var obj = {}
		obj.kembalian = money
		obj.list = []
		obj.memberId = memberId
		obj.money = money
		var sepatu = "Sepatu Stacattu"
		var zoro = "baju Zoro"
		var HNN = "baju HNN"
		var sweat = "Sweater uniklooh"
		var casing = "casing HP"
		var cek = 0 //supaya tidak beli casing yang banyak
			for(var i = 0; obj.kembalian>=50000 && cek == 0; i++){
	 		if(obj.kembalian >= 1500000){
	 			obj.kembalian -= 1500000
	 			obj.list.push(sepatu)
	 		}else if(obj.kembalian >= 500000){
	 			obj.kembalian -=500000
	 			obj.list.push(zoro)
	 		}else if(obj.kembalian >= 250000){
	 			obj.kembalian -= 250000
	 			obj.list.push(HNN)
	 		}else if(obj.kembalian >= 175000){
	 			obj.kembalian -= 175000
	 			obj.list.push(sweat)
	 		}else if(obj.kembalian >= 50000){
	 			if(cek = 1){
	 				obj.kembalian -= 50000
		 			obj.list.push(casing)
		 			cek +=1
	 			}else{
	 				break;
	 			}
		 	}
	 	
	 }

	// var output = "memberId : "+obj.memberId+", Money : "+obj.money+", List Purchased : "+obj.list+", Change Money : "+obj.kembalian
	// console.log(output)
 	}
 	return obj
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log("\nSoal 3\n")

function naikAngkot(listPenumpang){
	var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
	var arrOut = []
	if(listPenumpang.length <=0){
		return []
	}

	for(var i = 0; i<listPenumpang.length; i++){
		var obj ={}
		var asal = listPenumpang [i][1]
		var tujuan = listPenumpang[i][2]

		var indexAsal
		var indexTujuan
		for (var j = 0; j<rute.length; j++){
			if(rute[j]==asal){
				indexAsal =j
			}else if(rute [j] == tujuan){
				indexTujuan =j
			}
		}
		var bayar = (indexTujuan - indexAsal)* 2000

		obj.penumpang = listPenumpang[i][0]
		obj.naik = asal
		obj.turun = tujuan
		obj.bayar = bayar

		arrOut.push(obj)
	}return arrOut
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));