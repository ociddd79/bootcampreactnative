console.log("------------Nomor 1------------\n")
console.log("Looping pertama")

var i = 1;

while(i <= 20){
	if(i % 2 == 0){
		console.log(i + " - i love coding")
	}i++	
}

console.log("\nlooping kedua")

while(i > 0){
	if(i %2 == 0){
		console.log(i + " - i will become a mobile developer")
	}i--
}

console.log("\n------------nomor 2-------------")

for(var j = 1; j<=20; j++){
	if(j %2 == 1){
		if(j %3 == 0){
			console.log(j + " - i love coding");	
		}else{
			console.log(j + " - santai")
		}	
	}else if(j %2 == 0){
		console.log(j + " - berkualitas");
	}
}

console.log("\n------------nomor 3-------------")
var out = ""
 for(i = 1; i<=4; i++){
 	for(j = 1; j<=8; j++){
 		out += "#"
 	}
 	out+="\n"
 }
 console.log(out)

console.log("\n------------nomor 4-------------")
 
var i = 1
var hasil = ""
 while(i<=7){
 	hasil += "#"
 	console.log(hasil)
	i++; 
 } 	

console.log("\n------------nomor 5-------------")
var papan = ""
 for(i = 1; i<=8; i++){
 	for(j = 1; j<=4; j++){
 		if(i % 2 == 1){
 			papan +=" #"
 		}else{
 			papan += "# "	
 		}
 	}
 	papan +="\n"
 }
 console.log(papan)
